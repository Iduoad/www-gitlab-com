<!-- 
  Update the title of this issue to: Trainee BE maintainer (App) - [full name] 
  Where App can be:
  - GitLab
  - gitlab-shell
  - gitlab-workhorse
  - customers-license
  - version
  Or any supported project in the [Engineering Project](https://about.gitlab.com/handbook/engineering/projects/) list.
-->

## Basic setup

1. [ ] Read the [code review page in the handbook](https://about.gitlab.com/handbook/engineering/workflow/code-review/) and the [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
2. [ ] Understand [how to become a maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer) 
3. [ ] Create a merge request updating [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md)
       adding yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer)
4. [ ] Ask your manager to set up a check in on this issue every six weeks or so.
5. [ ] Join the `#trainee_maintainers` Slack channel.
6. [ ] _Optional:_ [Consider finding a mentor](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer-mentorship-pilot-program) to help you become a maintainer.

## Working towards becoming a maintainer

There is no checklist here, only guidelines. There is no specific timeline on
this, but historically most backend trainee maintainers have become maintainers
five to seven months after starting their training.

As part of your journey towards becoming a maintainer, you may find it useful to:

1. [ ] [Shadow a maintainer](#code-review-pairing) while they review an MR. This will allow you to get insight into the thought processes involved.
1. [ ] [Have a maintainer shadow _you_](#code-review-pairing) while you review an MR _as if you were a maintainer_ . Ideally, this would be with a different maintainer to the above, so you can get different insights.

You are free to discuss your progress with your manager or any
maintainer at any time. As in the list above, your manager should review
this issue with you roughly every six weeks; this is useful to track
your progress, and see if there are any changes you need to make to move
forward.

It is up to you to ensure that you are getting enough MRs to review, and of
varied types. All engineers are reviewers, so you should already be receiving
regular reviews from Reviewer Roulette. You could also seek out more reviews
from your team, or #backend Slack channels.

Your reviews should aim to cover maintainer responsibilities as well as reviewer
responsibilities. Your approval means you think it is ready to merge.

After each MR is merged or closed, add a discussion to this issue using this
template:

```markdown
### (Merge request title): (Merge request URL)

During review:

- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:

- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

(Maintainer who reviewed this merge request) Please add feedback, and compare
this review to the average maintainer review.
```

**Note:** Do not include reviews of security MRs because review feedback might
reveal security issue details.

**Tip:** There are [tools](https://about.gitlab.com/handbook/tools-and-tips/#trainee-maintainer-issue-upkeep) available to assist with this task.

### FAQ

  - **Should I ask for feedback for reviews on very small or trivial MRs that got merged without any additional feedback from maintainer?**
    - _Every trainee maintainer is encouraged to list all reviews, but may opt to not ask for maintainer feedback in trivial merge requests. This is also in sync with [being respectful of other's time](https://about.gitlab.com/handbook/communication/#be-respectful-of-others-time)_

### Code Review Pairing

Much like pair programming, pairing on code review is a great way to knowledge share and collaborate on merge request. This is a great activity for trainee maintainers to participate with maintainers for learning their process of code review.

A **private code review session** (unrecorded) involves one primary reviewer, and a shadow. If more than one shadow wishes to observe a private session, please consider obtaining consent from the merge request author.

A **public code review session** involves a primary reviewer and one or more shadows in a recorded session that is released publicly, for example to GitLab Unfiltered.
- If the merge request author is a GitLab team member, please consider obtaining consent from them.
- If the merge request author is a community contributor, you **must** obtain consent from them.
- Do **not** release reviews of security merge requests publicly.

## When you're ready to make it official

When reviews have accumulated, you can confidently address the majority of the MR's assigned to you, 
and recent reviews consistently fulfill maintainer responsibilities, then you can propose yourself as a new maintainer 
for the relevant application.

Remember that even when you are a maintainer, you can still request help from other maintainers if you come across an MR
that you feel is too complex or requires a second opinion. 

1. [ ] Create a merge request updating [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md) using [the template](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab/merge_request_templates/Backend%20maintainer.md) proposing yourself as a maintainer for the relevant application, assigned to your manager.
2. [ ] Keep reviewing, start merging :metal:

/label ~"trainee maintainer" ~backend
